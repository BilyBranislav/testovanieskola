package com.bilybranislav;

public class PesZachranar extends Pes {

    private String team;
    private int pocetAkcii;
    private int pocetUspesnychAkcii;

    public PesZachranar(String rasa, String meno, int vek, String farba, int vaha, String team, int pocetAkcii, int pocetUspesnychAkcii) {
        super(rasa, meno, vek, farba, vaha);
        this.team = team;
        this.pocetAkcii = pocetAkcii;
        this.pocetUspesnychAkcii = pocetUspesnychAkcii;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getPocetAkcii() {
        return pocetAkcii;
    }

    public void setPocetAkcii(int pocetAkcii) {
        this.pocetAkcii = pocetAkcii;
    }

    public int getPocetUspesnychAkcii() {
        return pocetUspesnychAkcii;
    }

    public void setPocetUspesnychAkcii(int pocetUspesnychAkcii) {
        this.pocetUspesnychAkcii = pocetUspesnychAkcii;
    }

    @Override
    public String toString() {
        return  super.toString() + "\n PesZachranar{" +
                "team='" + team + '\'' +
                ", pocetAkcii=" + pocetAkcii +
                ", pocetUspesnychAkcii=" + pocetUspesnychAkcii +
                '}';
    }

    public float uspesnostAkcii() {
        return (float) pocetUspesnychAkcii / (float)pocetAkcii * 100;
    }
}
