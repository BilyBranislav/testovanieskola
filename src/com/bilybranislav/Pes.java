package com.bilybranislav;

public class Pes {

    private String rasa;
    private String meno;
    private int vek;
    private String farba;
    private int vaha;

    public Pes(String rasa, String meno, int vek, String farba, int vaha) {
        this.rasa = rasa;
        this.meno = meno;
        this.vek = vek;
        this.farba = farba;
        this.vaha = vaha;
    }

    public String getRasa() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa = rasa;
    }

    public String getMeno() {
        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public int getVek() {
        return vek;
    }

    public void setVek(int vek) {
        this.vek = vek;
    }

    public String getFarba() {
        return farba;
    }

    public void setFarba(String farba) {
        this.farba = farba;
    }

    public int getVaha() {
        return vaha;
    }

    public void setVaha(int vaha) {
        this.vaha = vaha;
    }

    @Override
    public String toString() {
        return "Pes{" +
                "rasa='" + rasa + '\'' +
                ", meno='" + meno + '\'' +
                ", vek=" + vek +
                ", farba=" + farba +
                ", vaha=" + vaha +
                '}';
    }
}
