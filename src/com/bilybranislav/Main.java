package com.bilybranislav;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static ArrayList<Pes> psi;

    public static void main(String[] args) {
        String filePath = new File("").getAbsolutePath();
        filePath = filePath.concat("/src/com/bilybranislav/PsiVMeste.txt");
        psi = new ArrayList<>();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(filePath));
            String riadok;
            while ((riadok = bf.readLine()) != null) {
                Pes pes = vytvorPsaZRiadku(riadok);
                psi.add(pes);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Subor nenajdeny");
        } catch (IOException e) {
            e.printStackTrace();
        }
        uvitanie();
        userInput();
    }

    private static void userInput() {
        Scanner sc = new Scanner(System.in);
        String[] split = sc.nextLine().split(" ");
        int[] vyberVlasnosti = new int[split.length];
        String vlastnost = "";
        try {
            for (int i = 0; i < split.length; i++) {
                vyberVlasnosti[i] = Integer.parseInt(split[i]);
                if (vyberVlasnosti[i] < 1 || vyberVlasnosti[i] > 9) {
                    throw new NumberFormatException();
                }
            }
            //Ak spravny input, vypis zadane vlastnosti vsetkych psov
            for (Pes pes : psi) {
                for (int vlastnostIndex : vyberVlasnosti) {
                    vlastnost = vlastnost.concat(vypisVlastnost(pes, vlastnostIndex));
                }
                System.out.println(vlastnost);
                vlastnost = "";
            }
        } catch (NumberFormatException e) {
            System.out.println("Vyber si z vlastnosti");
        }
    }

    private static String vypisVlastnost(Pes pes, int i) {
            switch (i) {
                case 1:
                    return pes.getRasa() + " ";
                case 2:
                    return pes.getMeno()+ " ";

                case 3:
                    return pes.getVek() + " ";
                case 4:
                    return pes.getFarba()+ " ";
                case 5:
                    return pes.getVaha() + " ";
                case 6: {
                    if(pes instanceof PesZachranar) {
                        return ((PesZachranar) pes).getTeam()+ " ";
                    }
                }
                case 7: {
                    if(pes instanceof PesZachranar) {
                        return ((PesZachranar) pes).getPocetAkcii() + " ";
                    }
                }
                case 8: {
                    if(pes instanceof PesZachranar) {
                        return ((PesZachranar) pes).getPocetUspesnychAkcii() + " ";
                    }
                }
                case 9: {
                    if(pes instanceof PesZachranar) {
                        return ((PesZachranar) pes).uspesnostAkcii() + "%";
                    }
                }
            }
            return "";
    }

    private static void uvitanie() {
        System.out.print("1. rasa \n" +
                "2. meno \n" +
                "3. vek \n" +
                "4. farba \n" +
                "5. vaha \n" +
                "6. team \n" +
                "7. pocetAkcii \n" +
                "8. pocetUspesnychAkcii \n" +
                "9. uspesnostAkcii \n"
        );
    }

    private static Pes vytvorPsaZRiadku(String riadok) {
        String[] vlastnosti = riadok.split(" ");
        Pes pes;
        if (vlastnosti.length == 5) {
            pes = new Pes(vlastnosti[0], vlastnosti[1],
                    Integer.parseInt(vlastnosti[2]), vlastnosti[3],
                    Integer.parseInt(vlastnosti[4]));
        } else {
            pes = new PesZachranar(vlastnosti[0], vlastnosti[1],
                    Integer.parseInt(vlastnosti[2]), vlastnosti[3],
                    Integer.parseInt(vlastnosti[4]),
                    vlastnosti[5], Integer.parseInt(vlastnosti[6]),
                    Integer.parseInt(vlastnosti[7]));
        }
        return pes;
    }
}
